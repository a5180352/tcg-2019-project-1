#!/usr/bin/env python3

"""
Basic framework for developing 2048 programs in Python

Author: Hung Guei (moporgic)
        Computer Games and Intelligence (CGI) Lab, NCTU, Taiwan
        http://www.aigames.nctu.edu.tw
"""

from board import board
from action import action
import random

class agent:
    """ base agent """

    def __init__(self, options = ""):
        self.info = {}
        options = "name=unknown role=unknown " + options
        for option in options.split():
            data = option.split("=", 1) + [True]
            self.info[data[0]] = data[1]
        return

    def open_episode(self, flag = ""):
        return

    def close_episode(self, flag = ""):
        return

    def take_action(self, state):
        return action()

    def check_for_win(self, state):
        return False

    def property(self, key):
        return self.info[key] if key in self.info else None

    def notify(self, message):
        data = message.split("=", 1) + [True]
        self.info[data[0]] = data[1]
        return

    def name(self):
        return self.property("name")

    def role(self):
        return self.property("role")

class random_agent(agent):
    """ base agent for agents with random behavior """

    def __init__(self, options = ""):
        super().__init__(options)
        seed = self.property("seed")
        if seed is not None:
            random.seed(int(seed))
        self.rstate = random.getstate()
        return

    def choice(self, seq):
        random.setstate(self.rstate)
        target = random.choice(seq)
        self.rstate = random.getstate()
        return target

    def shuffle(self, seq):
        random.setstate(self.rstate)
        random.shuffle(seq)
        self.rstate = random.getstate()
        return

class rndenv(random_agent):
    """
    random environment
    add a new random tile to an empty cell with bagging rule
    """

    def __init__(self, options = ""):
        super().__init__("name=random role=environment " + options)
        self.bag = [] # zr
        return

    # Threes!
    def take_action(self, state):
        empty = [pos for pos, tile in enumerate(state.state) if tile == -1]
        if empty:
            pos = self.choice(empty)
            for index in empty:
                state.state[index] = 0
            if not self.bag:
                self.bag = [1, 2, 3]
            # print('bag', self.bag) # zr
            tile = self.choice(self.bag)
            self.bag.remove(tile)
            return action.place(pos, tile)
        else:
            return action()

    def init_move(self, state):
        self.bag = init_moves = []
        empty_pos = [i for i in range(16)]
        for i in range(9):
            pos = self.choice(empty_pos)
            empty_pos.remove(pos)
            if not self.bag:
                self.bag = [1, 2, 3]
            tile = self.choice(self.bag)
            self.bag.remove(tile)
            init_moves += [action.place(pos, tile)]
        return init_moves

class player(random_agent):
    """
    dummy player
    select a legal action randomly
    """

    def __init__(self, options = ""):
        super().__init__("name=dummy role=player " + options)
        return

    def take_action(self, state):
        best_op, max_reward = None, -1
        for op in range(4):
            if board(state).slide(op) > max_reward:
                best_op = op
                max_reward = board(state).slide(op)
        # # zr
        # res = ["#U", "#R", "#L", "#D", "#?"]
        # print([res[i] for i in legal])
        if max_reward != -1:
            return action.slide(best_op)
        else:
            return action()

if __name__ == '__main__':
    print('2048 Demo: agent.py\n')

    state = board()
    env = rndenv()
    ply = player()

    a = env.take_action(state)
    r = a.apply(state)
    print(a)
    print(r)
    print(state)

    a = env.take_action(state)
    r = a.apply(state)
    print(a)
    print(r)
    print(state)

    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))

    state = board()
    state[0] = 1
    state[1] = 1
    print(state)

    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(ply.take_action(state))
    print(state)
